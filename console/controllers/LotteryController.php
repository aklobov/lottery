<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
//use yii\helpers\Console;
use common\components\lottery\models\BaseLottery;
use common\components\lottery\models\L6x45;
use common\components\lottery\models\L6x45Ticket;
use common\components\lottery\models\L6x45Draw;
use yii\base\UserException;

/**
 *
 */
class LotteryController extends Controller
{

    /**
     * Draw lottery 6 out of 45
     * - with defined id
     * or
     * - draw current active lottery (if id not defined)
     * @param integer $id Draw number (optional)
     */
    public function actionDraw6x45($id = null)
    {
        if (is_numeric($id)) {
            $lottery = L6x45::findOne($id);
        }
        else {
            $lottery = L6x45::findNearest();
        }
        if (!($lottery instanceof BaseLottery)) {
            throw new UserException('Lottery not found');
        }
        if (!$lottery->enabled) {
            throw new UserException('Active lottery not found');
        }
        if (!empty($lottery->draw)) {
            throw new UserException('Lottery was drawed yet');
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (($lottery instanceof BaseLottery) && (bool) $lottery->draw()) {
            $lottery->pool = L6x45Ticket::getPool($lottery);
            L6x45Draw::checkTickets($lottery);
            if (L6x45Draw::payoutBets($lottery) || is_null($lottery->pool)) {
                $transaction->commit();

                //Create lotteries
                $cnt       = L6x45::findFutureLotteriesCount();
                $neededCnt = Yii::$app->keyStorage->get('lottery.common.multidraw_count') - $cnt;
                if ($neededCnt > 0) {
                    $lastLottery = L6x45::findLastInFuture();
                    $period      = Yii::$app->keyStorage->get('lottery.6x45.interval');
                    $res         = L6x45::createSet($lastLottery->draw_at + $period, $neededCnt, $period);
                    //TODO Внести событие создания в лог
                }
                return Controller::EXIT_CODE_NORMAL;
            }
        }
        $transaction->rollBack();
        if ($lottery instanceof BaseLottery) {
            Yii::error($lottery->getErrors());
        }
        return Controller::EXIT_CODE_ERROR;
    }

    /**
     * Check wallet's amount and transfer glut of money to backup wallet.
     * Have no parameters.
     */
    public function actionWalletLimit()
    {
        $balance = Yii::$app->pm->balance();
        $limit   = Yii::$app->keyStorage->get('payment.pm.account_max_limit', 1e3);
        if ($balance > $limit) {
            $payment = Yii::app()->createController('payment');
            //http://ru.stackoverflow.com/questions/216860/%D0%9A%D0%B0%D0%BA-%D0%B2%D1%8B%D0%B7%D0%B2%D0%B0%D1%82%D1%8C-%D0%BC%D0%B5%D1%82%D0%BE%D0%B4-%D0%B8%D0%B7-%D0%B4%D1%80%D1%83%D0%B3%D0%BE%D0%B3%D0%BE-%D0%BA%D0%BE%D0%BD%D1%82%D1%80%D0%BE%D0%BB%D0%BB%D0%B5%D1%80%D0%B0
            $res     = $payment->handlePmPayout(Yii::$app->params['pm.backup.wallet'], $balance - $limit);
        }

        $balance = Yii::$app->payeer->balance();
        $limit   = Yii::$app->keyStorage->get('payment.pr.account_max_limit', 1e3);
        if ($balance > $limit) {
            $payment = Yii::app()->createController('payment');
            $res     = $payment->handlePrPayout(Yii::$app->params['pr.backup.wallet'], $balance - $limit);
        }
    }

}
