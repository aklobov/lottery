<?php

return [
    'id' => 'tlgrm',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'tlgrm\controllers',
    'components' => [
        'urlManager' => require(__DIR__ . '/_urlManager.php'),
    ],
    'params' => [
        'money' => require 'money.php',
        'routes' => require 'route.php',
        'games' => require 'games.php',
    ],
];
