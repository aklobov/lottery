<?php
return [
    'test' => [
        'enabled' => true,
        'canadd' => true,
        'candraw' => false,
        'alink' => false,
        'dlink' => false,
        // 'linktext' => '',
    ],
    'pm' => [
        'enabled' => true,
        'canadd' => true,
        'candraw' => true,
        'alink' => '',
        'dlink' => '',
        // 'linktext' => '',
    ],
    'bc' => [
        'enabled' => true,
        'canadd' => true,
        'candraw' => true,
        'alink' => '',
        'dlink' => '',
        // 'linktext' => '',
    ],
];