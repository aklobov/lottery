<?php
/**
 * Наборы кнопок и текстов для каждого "роута"
 * ключи связаны с соответствующими языками
 */
return [
    // начало
    'start' => [
        'text' => 'starttext',
        'buttons' => [
                        'play',
                        'howto',
                        'money',
                        'config',
                        'feedback',
                        'partner',
                        // 'util',
                    ],
    ],


    ////////////////////// деньги
    'money' => [
        'text' => 'moneytext',
        'buttons' => [
                        'money.put',
                        'money.get',
                        'back',
                    ],
    ],

    'money.put' => [
        'text' => 'moneyputtext',
        'buttons' => [
                        '@@tm',
                        '@@pm',
                        '@@bc',
                        'back',
                        'start',

                    ],
    ],

    'money.get' => [
        'text' => 'moneygettext',
        'buttons' => [
                        // 'run',
                        'back',
                        'start',

                    ],
    ],


    ////////////////////// игры
    'play' => [
        'text' => 'playtext',
        'buttons' => [
                        'play.1f3',
                        'play.3f9',
                        'play.roul',
                        'back',
                    ],
    ],

    'play.1f3' => [
        'text' => '1f3text',
        'buttons' => [
                        'run',
                        'back',
                    ],
    ],

    'play.3f9' => [
        'text' => '3f9text',
        'buttons' => [
                        'run',
                        'back',
                    ],
    ],

    //////////// инструкции
    'howto' => [
        'text' => 'howtotext',
        'buttons' => [
                        'back',
                    ],
    ],

    //////////// доп. утилиты
    // 'util' => [
    //     'text' => 'utiltext',
    //     'buttons' => [
    //                     'util.balance',
    //                     'util.decode',
    //                     'back',
    //                 ],
    // ],
    // // глянуть на состояние баланса
    // 'util.balance' => [
    //     'text' => 'utilbaltext',
    //     'buttons' => [
    //                     'back',
    //                     'refresh',
    //                     'start',
    //                 ],
    // ],
    // // расшифровать загаданные числа
    // 'util.decode' => [
    //     'text' => 'utildectext',
    //     'buttons' => [
    //                     'back',
    //                     'start',
    //                 ],
    // ],


    ////////////////// обратная связь
    'feedback' => [
        'text' => 'feedbacktext',
        'buttons' => [
                        'back',
                    ],
    ],
    // обратная связь, сообщение отправлено
    'feedbackaftersend' => [
        'text' => 'feedbacktextaftersend',
        'buttons' => [
                        'back',
                    ],
    ],




    ////////////////// config
    'config' => [
        'text' => 'configtext',
        'buttons' => [
                        'back',
                    ],
    ],
    // обратная связь, сообщение отправлено
    // 'feedbackaftersend' => [
    //     'text' => 'feedbacktextaftersend',
    //     'buttons' => [
    //                     'back',
    //                 ],
    // ],


    ///////////// партнерка старт
    'partner' => [
        'text' => 'partnertext',
        'buttons' => [
                        'partner.refgelink',
                        'partner.refstat',
                        'back',
                    ],
    ],

    // партнерка получение ссылки
    'partner.refgelink' => [
        'text' => 'refgelinktext',
        'buttons' => [
                        'refresh',
                        'partner.refstat',
                        'back',
                    ],
    ],

    // партнерка получение статистики
    'partner.refstat' => [
        'text' => 'refstattext',
        'buttons' => [
                        'partner.refgelink',
                        'refresh',
                        'back',
                    ],
    ],
];