<?php

namespace tlgrm\controllers;

use Yii;
use yii\web\Controller;
use Longman\TelegramBot;
use common\models\UserProfile;

/**
 * Telegram bot for FreedomLOTTO
 */
class SiteController extends Controller
{

    public $layout = null;

    /**
     * http://site.com
     */
    public function actionIndex()
    {
        echo Yii::$app->id;
        echo Yii::$app->params['games']['1x3']['enabled'];
//        print_r(Yii::$app->params);

        $userProfile = UserProfile::find()->byTelegramId(111)->one();
        $userProfile->account += 100;
        $userProfile->save();
        echo \yii\helpers\VarDumper::dump($userProfile);
        return;
    }

    /**
     * http://site.com/index.php/site/test
     * @return integer
     */
    public function actionTest()
    {
        $bot = new TelegramBot;

        $user           = new User;
        $user->username = 'Ivan';
        if (!$user->save()) {
            throw new Exception("Ошибка сохранения профиля пользователя.");
        };

        $profileData = [
            'phone' => '12312',
            'account' => Yii::$app->keyStorage->get('user.account.default'),
        ];
        $user->afterSignup($profileData);
        return Controller::EXIT_CODE_ERROR;
    }

}
