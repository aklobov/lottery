<?php

namespace backend\controllers;

use Yii;
use common\models\UserAccountStat;
use backend\models\search\UserAccountStatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PaymentForm;
use frontend\controllers\PaymentController;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * UserAccountStatController implements the CRUD actions for UserAccountStat model.
 */
class UserAccountStatController extends Controller
{
    /*
      public function behaviors()
      {
      return [
      'verbs' => [
      'class' => VerbFilter::className(),
      'actions' => [
      'delete' => ['post'],
      ],
      ],
      ];
      }
     */

    /**
     * Lists all UserAccountStat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $action    = Yii::$app->request->post('action');
        $selection = (array) Yii::$app->request->post('selection');
        foreach ($selection as $id) {
            $model = UserAccountStat::findOne((int) $id);
            $res   = true;
            if (
                    !empty($model) &&
                    in_array($model->status, [UserAccountStat::STATUS_POSTPONED, UserAccountStat::STATUS_ERRORED,]) &&
                    $model->direction == UserAccountStat::DIRECTION_OUT
            ) {
                if ($action == UserAccountStat::STATUS_APPROVED) {
                    switch ($model->system) {
                        case PaymentForm::$methods[PaymentForm::PAYMENT_PERFECTMONEY]:
                            $res = $this->_PmPayout($model->target, $model->amount, $model);
                            break;
                        case PaymentForm::$methods[PaymentForm::PAYMENT_PAYEER]:
                            $res = $this->_PrPayout($model->target, $model->amount, $model);
                            break;

                        default:
                            break;
                    }
                } elseif ($action == UserAccountStat::STATUS_CANCELLED) {
//                    $model->status      = UserAccountStat::STATUS_CANCELLED;
                    $model->description = 'Операция отклонена оператором';
                    $model->cancel();
//                    $model->save();
                }
            } else {
                $res = false;
            }
            if (!$res) {
                $message = sprintf('Перевод денег не выполнен. Операция: <br>Номер: [%d], <br>Платежная система: [%s], <br>Сумма: [%s].', $model->id, $model->system, $model->amount);
                Yii::$app->session->addFlash('warning', '<hr>' . $message);
            }
        }

        $searchModel        = new UserAccountStatSearch();
        $dataProvider       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['id' => SORT_DESC]
        ];

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport()
    {
        $searchModel        = new UserAccountStatSearch();
        $dataProvider       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['id' => SORT_DESC]
        ];
        return $this->render('export', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserAccountStat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserAccountStat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
      public function actionCreate()
      {
      $model = new UserAccountStat();

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
      } else {
      return $this->render('create', [
      'model' => $model,
      ]);
      }
      }
     */

    /**
     * Updates an existing UserAccountStat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*
      public function actionUpdate($id)
      {
      $model = $this->findModel($id);

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
      } else {
      return $this->render('update', [
      'model' => $model,
      ]);
      }
      }
     */

    /**
     * Deletes an existing UserAccountStat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*
      public function actionDelete($id)
      {
      $this->findModel($id)->delete();

      return $this->redirect(['index']);
      }
     */

    /**
     * Finds the UserAccountStat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserAccountStat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserAccountStat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function _PrPayout($target, $amount, UserAccountStat $stat)
    {
        $balance   = Yii::$app->payeer->balance();
        $amountMax = isset($balance['balance']['USD']['DOSTUPNO_SYST']) ? $balance['balance']['USD']['DOSTUPNO_SYST'] : null;
        $amount    = YII_ENV == 'dev' && $amount > $amountMax ? 0.02 : $amount; //FIXME 
        $amountMax *= YII_ENV == 'dev' ? 100000 : 1;
        if ($amount > $amountMax) {
            $message           = sprintf('На счету платежной системы Payeer недостаточно средств для выполнения заявки [%d] Запрошено: %s. Доступно: %s', $stat->id, $amount, $amountMax);
            $stat->description = $message;
            $stat->save();
            Yii::$app->session->addFlash('warning', '<hr>' . $message);
            return false;
        }
        $memo = sprintf('Вывод средств с сайта %s пользователя %s [%d]', Yii::$app->name, $stat->user->publicIdentity, $stat->user_id);
        try {
            $amountPay = YII_ENV == 'dev' ? $amount / 100000 : $amount;
//            $amountPay = YII_ENV == 'dev' ? 0.02 : $amount;
            $amountPay = Yii::$app->formatter->asDecimal($amountPay, 2);
            $res       = Yii::$app->payeer->transfer($target, $amountPay, time(), $memo);
            $re        = false;
            if (isset($res['historyId']) && $res['historyId'] > 0) {
                $stat->operation_id = $res['historyId'];
                $stat->status       = UserAccountStat::STATUS_APPROVED;
                $stat->description  = $memo;
                Yii::info(print_r($res, 1) . ' ' . $memo, 'payment\payeer\payout\result');
                $re                 = true;
            } else {
                $stat->status      = UserAccountStat::STATUS_ERRORED;
                $stat->description = implode(' ', Yii::$app->payeer->getErrors());
                $message           = sprintf('<h3>Payeer</h3>Ошибка выполнения заявки [%d]. <pre>%s</pre>', $stat->id, $stat->description);
                Yii::$app->session->addFlash('warning', '<hr>' . $message);
                Yii::error($res, 'payment\payeer\payout\error');
                Yii::error(Yii::$app->payeer->getErrors(), 'payment\payeer\payout\error');
            }
        } catch (UnprocessableEntityHttpException $e) {
            $message = sprintf('Ошибка платежной системы. %s', $e->getMessage());
            Yii::error($message, 'payment\payeer\payout\error');
        } finally {
            $stat->save();
        }
        return $re;
    }

    protected function _PmPayout($target, $amount, UserAccountStat $stat)
    {
        $balance   = Yii::$app->pm->balance();
        $amountMax = isset($balance[Yii::$app->pm->walletNumber]) ? $balance[Yii::$app->pm->walletNumber] : null;
//        $amount    = YII_ENV == 'dev' && $amount > $amountMax ? 0.01 : $amount;
        $amountMax *= YII_ENV == 'dev' ? 100000 : 1;
        if ($amount > $amountMax) {
            $message           = sprintf('На счету платежной системы Perfect Money недостаточно средств для выполнения заявки [%d]. Запрошено: %s. Доступно: %s.', $stat->id, $amount, $amountMax);
            $stat->description = $message;
            $stat->save();
            Yii::$app->session->addFlash('warning', '<hr>' . $message);
            return false;
        }
        $memo = sprintf('Вывод средств с сайта %s пользователя %s [%d]', Yii::$app->name, $stat->user->publicIdentity, $stat->user_id);
        try {
            $re        = false;
            $amountPay = YII_ENV == 'dev' ? $amount / 100000 : $amount;
            $amountPay = Yii::$app->formatter->asDecimal($amountPay, 2);
            $res       = Yii::$app->pm->transfer($target, $amountPay, time(), $memo);
            if (isset($res['PAYMENT_BATCH_NUM'])) {
                $stat->operation_id = $res['PAYMENT_BATCH_NUM'];
                $stat->status       = UserAccountStat::STATUS_APPROVED;
                $stat->description  = $memo;
                Yii::info(print_r($res, 1) . ' ' . $memo, 'payment\pm\payout\result');
                $re                 = true;
            } else {
                $stat->status      = UserAccountStat::STATUS_ERRORED;
                $stat->description = ArrayHelper::getValue($res, 'ERROR', 'Ошибка вывода средств');
                $message           = sprintf('<h3>Perfect Money</h3>Ошибка выполнения заявки [%d]. <pre>%s</pre>', $stat->id, $stat->description);
                Yii::$app->session->addFlash('warning', '<hr>' . $message);
                Yii::error($res, 'payment\pm\payout\error');
            }
        } catch (UnprocessableEntityHttpException $e) {
            $message = sprintf('Ошибка платежной системы. %s', $e->getMessage());
            Yii::error($message, 'payment\pm\payout\error');
        } catch (Exeption $ex) {
            Yii::error($ex);
        } finally {
            $stat->save();
        }
        return $re;
    }

}
