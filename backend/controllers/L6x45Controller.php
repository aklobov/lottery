<?php

namespace backend\controllers;

use Yii;
use common\components\lottery\models\L6x45;
use common\components\lottery\models\L6x45Search;
use backend\models\LotterySetForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * L6x45Controller implements the CRUD actions for L6x45 model.
 */
class L6x45Controller extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all L6x45 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new L6x45Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single L6x45 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new L6x45 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new L6x45([
            'enabled' => true,
//            'superprize' => Yii::$app->keyStorage->get('lottery.6x45.superprize.default')
        ]);
//        $model->enabled = 1;
//        $model->superprize = Yii::$app->keyStorage->get('lottery.6x45.superprize.default');
        //TODO Однообразно создавать лотереи (через статический метод? И чтобы он возвращал true false
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new L6x45 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateSet()
    {
        $model = new LotterySetForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            L6x45::createSet($model->firstDrawAt, $model->count, 86400 * $model->period);
            return $this->redirect(['index']);
        } else {
            return $this->render('createSet', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing L6x45 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing L6x45 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Toggle state of existing Lottery model.
     * If toggle is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionToggle($id, $view = 'index')
    {
        $model = $this->findModel($id)->toggle();
        if ($model->save()) {
            Yii::$app->session->setFlash('info', "Lottery draw № <strong>{$model->id}</strong> status changed successfully.");
        } else {
            Yii::$app->session->setFlash('error', "Lottery draw № <strong>{$model->id}</strong> status change error.<br /><pre>" . print_r($model->getErrors(), 1) . '</pre>');
        }
        return $view == 'view' ? $this->render($view, ['model' => $this->findModel($id)]) : $this->redirect($view);
    }

    /**
     * Finds the L6x45 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return L6x45 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = L6x45::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
