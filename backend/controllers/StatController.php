<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use cheatsheet\Time;
use common\components\lottery\models\L1x3Ticket;
use common\components\lottery\models\L3x9Ticket;
use common\components\lottery\models\L6x45Ticket;
use common\components\slots\models\SlotsFiveTicket;
use backend\models\AccountLog;

/**
 * StatController - статистическая информация.
 */
class StatController extends Controller
{

    /**
     * Lists all Stat.
     * @return mixed
     */
    public function actionIndex()
    {
        // 1 из 3
        $l1x3Stat                           = [];
        $l1x3Stat['day']['bets_count']      = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->count('id');
        $l1x3Stat['day']['bets_paid']       = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid');
        $l1x3Stat['day']['bets_paid_out']   = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid_out');
        $l1x3Stat['week']['bets_count']     = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->count('id');
        $l1x3Stat['week']['bets_paid']      = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid');
        $l1x3Stat['week']['bets_paid_out']  = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid_out');
        $l1x3Stat['month']['bets_count']    = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->count('id');
        $l1x3Stat['month']['bets_paid']     = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid');
        $l1x3Stat['month']['bets_paid_out'] = L1x3Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid_out');
        // 3 из 9
        $l3x9Stat                           = [];
        $l3x9Stat['day']['bets_count']      = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->count('id');
        $l3x9Stat['day']['bets_paid']       = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid');
        $l3x9Stat['day']['bets_paid_out']   = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid_out');
        $l3x9Stat['week']['bets_count']     = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->count('id');
        $l3x9Stat['week']['bets_paid']      = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid');
        $l3x9Stat['week']['bets_paid_out']  = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid_out');
        $l3x9Stat['month']['bets_count']    = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->count('id');
        $l3x9Stat['month']['bets_paid']     = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid');
        $l3x9Stat['month']['bets_paid_out'] = L3x9Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid_out');
        
        // Слоты Пятерочка
        $slotsFiveStat                           = [];
        $slotsFiveStat['day']['bets_count']      = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->count('id');
        $slotsFiveStat['day']['bets_paid']       = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid');
        $slotsFiveStat['day']['bets_paid_out']   = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid_out');
        $slotsFiveStat['week']['bets_count']     = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->count('id');
        $slotsFiveStat['week']['bets_paid']      = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid');
        $slotsFiveStat['week']['bets_paid_out']  = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid_out');
        $slotsFiveStat['month']['bets_count']    = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->count('id');
        $slotsFiveStat['month']['bets_paid']     = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid');
        $slotsFiveStat['month']['bets_paid_out'] = SlotsFiveTicket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid_out');

        // 6 из 45
        $l6x45Stat                           = [];
        $l6x45Stat['day']['bets_count']      = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->count('id');
        $l6x45Stat['day']['bets_paid']       = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid');
        $l6x45Stat['day']['bets_paid_out']   = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_DAY])
                ->sum('paid_out');
        $l6x45Stat['week']['bets_count']     = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->count('id');
        $l6x45Stat['week']['bets_paid']      = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid');
        $l6x45Stat['week']['bets_paid_out']  = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_WEEK])
                ->sum('paid_out');
        $l6x45Stat['month']['bets_count']    = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->count('id');
        $l6x45Stat['month']['bets_paid']     = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid');
        $l6x45Stat['month']['bets_paid_out'] = L6x45Ticket::find()
                ->where(['>', 'created_at', time() - Time::SECONDS_IN_A_MONTH])
                ->sum('paid_out');

        //Платежные системы
        $query            = AccountLog::find()
                ->where(['like', 'prefix', 'payment']);
//                ->andWhere(['or like', 'category', ['withdraw', 'charge']])
//                ->andWhere(['level' => 4]);
//        print_r($query->createCommand()->rawSql);
        $q1               = clone $query;
        $res              = $q1->andWhere(['>', 'log_time', time() - Time::SECONDS_IN_A_DAY])
                ->all();
//        print_r($res);die;
        $payStat['day']   = $this->_parsePayStat($res);
        $q2               = clone $query;
        $res              = $q2->andWhere(['>', 'log_time', time() - Time::SECONDS_IN_A_WEEK])
                ->all();
        $payStat['week']  = $this->_parsePayStat($res);
        $q3               = clone $query;
        $res              = $q3->andWhere(['>', 'log_time', time() - Time::SECONDS_IN_A_MONTH])
                ->all();
        $payStat['month'] = $this->_parsePayStat($res);

        return $this->render('index', [
                    'l1x3Stat' => $l1x3Stat,
                    'l3x9Stat' => $l3x9Stat,
                    'slotsFiveStat' => $slotsFiveStat,
                    'l6x45Stat' => $l6x45Stat,
                    'payStat' => $payStat,
        ]);
    }

    protected function _parsePayStat(array $stat)
    {
        $resS = [
            'perfectmoney' => [
                'payment' => 0,
                'payout' => 0
            ],
            'payeer' => [
                'payment' => 0,
                'payout' => 0
            ],
            'fchange' => [
                'payment' => 0,
                'payout' => 0
            ],
        ];
        foreach ($stat as $item) {
            if (strpos($item->message, 'Perfect Money')) {
                preg_match_all('/\[([\d\.]+)\]/', $item->message, $res);
                $amount = isset($res[1][1]) ? $res[1][1] : 0;
                if (strpos($item->prefix, 'payout') && strpos($item->message, 'Perfect Money payout')) {
                    $resS['perfectmoney']['payout'] += $amount;
                } elseif (strpos($item->prefix, 'payout') && strpos($item->message, 'Perfect money transaction rollback')) {
                    $resS['perfectmoney']['payout'] -= $amount;
                } elseif (strpos($item->prefix, 'pm-status') && strpos($item->message, 'Perfect Money incoming payment')) {
                    $resS['perfectmoney']['payment'] += $amount;
                }
            }
            if (strpos($item->message, 'Payeer')) {
                preg_match_all('/\[([\d\.]+)\]/', $item->message, $res);
                $amount = isset($res[1][1]) ? $res[1][1] : 0;
                if (strpos($item->prefix, 'payout') && strpos($item->message, 'Payeer payout')) {
                    $resS['payeer']['payout'] += $amount;
                } elseif (strpos($item->prefix, 'payout') && strpos($item->message, 'Payeer transaction rollback')) {
                    $resS['payeer']['payout'] -= $amount;
                } elseif (strpos($item->prefix, 'pr-status') && strpos($item->message, 'Payeer incoming payment')) {
                    $resS['payeer']['payment'] += $amount;
                }
            }
            if (strpos($item->message, 'F-Change')) {
                preg_match_all('/\[([\d\.]+)\]/', $item->message, $res);
                $amount = isset($res[1][1]) ? $res[1][1] : 0;
                if (strpos($item->prefix, 'payout') && strpos($item->message, 'F-Change payout')) {
                    $resS['fchange']['payout'] += $amount;
                } elseif (strpos($item->prefix, 'payout') && strpos($item->message, 'F-Change transaction rollback')) {
                    $resS['fchange']['payout'] -= $amount;
                } elseif (strpos($item->prefix, 'fchange-status') && strpos($item->message, 'F-Change incoming payment')) {
                    $resS['fchange']['payment'] += $amount;
                }
            }
        }
        return $resS;
    }

    /**
     * Finds the SystemLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SystemLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentLog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
