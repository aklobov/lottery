<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\UserForm;
use backend\models\search\UserSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use common\components\lottery\models\L5x36;
//use common\components\lottery\l5x36\Lottery;
use yii\base\UserException;
use yii\httpclient\Client;
use Google\Authenticator\GoogleAuthenticator;

/**
 * UserController implements the CRUD actions for User model.
 */
class TestController extends Controller
{

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $randomConfig                 = [
            'count' => 6,
            'max' => 45
        ];
        Yii::$app->random->attributes = $randomConfig;
        return $this->render('index', [
//                    'model' => Yii::$app->random->getNumbers(),
//                    'model' => Yii::$app->random->numbers,
//            'model'=>  L5x36::findCurrent()
//            'model'=>  L5x36::findLastFinished()
                    'model' => L5x36::findLastInFuture()
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //FIXME Remove in production
    public function actionCreateSet()
    {
        $lotteriesSet = Lottery::createSet();
        return $this->render('createSet', [
                    'model' => $lotteriesSet,
        ]);
    }

    public function actionCreateBets()
    {
        $lBets = Lottery::_createBets(20);
        return $this->render('createBets', [
                    'model' => $lBets,
        ]);
    }

    public function actionDraw()
    {
        $l = Lottery::Draw();
        return $this->render('draw', [
                    'model' => $l,
        ]);
    }

    public function actionPool()
    {
        $l = Lottery::getPool(L5x36::findLastFinished());
//        $l = L
        return $this->render('pool', [
                    'model' => $l,
        ]);
    }

    public function actionCalcWins()
    {
        $l = L5x36::findLastFinished();
        Lottery::checkBets($l);
        Lottery::payOutBets($l);
        return $this->render('wins', [
                    'model' => $l,
        ]);
    }

    public function actionTest()
    {
        $ga     = new GoogleAuthenticator();
//        $secret = $ga->generateSecret();
        $secret = 'EZYSDVMYHNNYHH3P';
        $resp   = 688741;
        echo $url    = $ga->getUrl('tesdt', 'mysite.com', $secret);
        echo '<br>';
        echo $res    = $ga->getCode($secret);
        die();
    }

    public function actionTest0()
    {
        $mailer  = Yii::$app->mailer;
//        $mailer  = Yii::$app->mail;
        print_r($mailer);
        die;
        $message = $mailer->compose()
                ->setFrom('no-reply@freedom-lotto.com')
                ->setTo('kagpost@ya.ru')
                ->setSubject('Проверка связи')
                ->setTextBody('message body sample Нужно купить слона');
//     ->send();
        $logger  = new Swift_Plugins_Loggers_ArrayLogger();
        $mailer->getSwiftMailer()->registerPlugin(new Swift_Plugins_LoggerPlugin($logger));
        if (!$message->send()) {
            echo $logger->dump();
        } else {
            echo 'success';
            echo $logger->dump();
        }
    }

    /*
      public function actionTest2()
      {
      $id = 61;
      if (is_numeric($id)) {
      $lottery = L6x45::findOne($id);
      }
      else {
      $lottery = L6x45::findNearest();
      }
      if (!($lottery instanceof BaseLottery)) {
      throw new UserException('Lottery not found');
      }
      if (!$lottery->enabled) {
      throw new UserException('Active lottery not found');
      }
      //        $transaction = Yii::$app->db->beginTransaction();
      if (($lottery instanceof BaseLottery) && (bool) $lottery->draw()) {
      $lottery->draw = '[6,7,29,20,23,34]';
      $lottery->pool = L6x45Ticket::getPool($lottery);
      L6x45Draw::checkTickets($lottery);
      if (L6x45Draw::payoutBets($lottery) || is_null($lottery->pool)) {
      //                $transaction->commit();
      //Create lotteries
      $cnt       = L6x45::findFutureLotteriesCount();
      $neededCnt = Yii::$app->keyStorage->get('lottery.common.multidraw_count') - $cnt;
      if ($neededCnt > 0) {
      $lastLottery = L6x45::findLastInFuture();
      $period      = Yii::$app->keyStorage->get('lottery.6x45.interval');
      $res         = L6x45::createSet($lastLottery->draw_at + $period, $neededCnt, $period);
      //TODO Внести событие создания в лог
      }
      return 0;
      }
      }
      $transaction->rollBack();
      if ($lottery instanceof BaseLottery) {
      Yii::error($lottery->getErrors());
      }
      return 1;
      }

      public function actionTest3()
      {
      $res = User::find()->refCount(3)->scalar();
      print_r($res);
      }

      public function actionCreateBets()
      {
      $res = L6x45::_createBets();
      echo '<pre>';
      print_r($res);
      echo '</pre>';
      }

      public function actionTest4()
      {
      return 'sad';
      }

      /**
     * Проверка превышения балансом предела и перевод излишка на запасной счёт.
     * Для тестирования.
     */
    /*
      public function actionTestPayment()
      {
      $balance = Yii::$app->pm->balance();
      $limit   = Yii::$app->keyStorage->get('payment.pm.account_max_limit', 1e3);
      if ($balance > $limit) {
      $payment = Yii::app()->createController('payment');
      $res = $payment->handlePmPayout(Yii::$app->params['pm.backup.wallet'], $balance - $limit);
      }

      $balance   = Yii::$app->payeer->balance();
      $limit   = Yii::$app->keyStorage->get('payment.pr.account_max_limit', 1e3);
      if ($balance > $limit) {
      $payment = Yii::app()->createController('payment');
      $res = $payment->handlePrPayout(Yii::$app->params['pr.backup.wallet'], $balance - $limit);
      }

      }
     */

    public function actionTestPost()
    {

        $client   = new Client(
                [
//            'requestConfig' => [
//            ],
            'transport' => 'yii\httpclient\CurlTransport',
//            'transport'     => 'yii\httpclient\StreamTransport',
                ]
        );
        $response = $client->createRequest()
                ->setMethod('post')
//                ->setUrl('http://euro-lotto.club/payment/fchange-status')
//                ->setUrl('http://frontend.lottery/payment/fchange-status')
//                ->setUrl('https://simple.freedom-lotto.com/payment/fchange-status')
                ->setUrl('https://freedom-lotto.com/payment/fchange-status')
                ->setData([
                    'merchant_name' => 'euro-lotto',
                    'merchant_title' => 'EuroLOTTO',
                    'payed_paysys' => 'YAMRUB',
                    'amount' => 0.01,
                    'payment_info' => 'Пополнение счёта EuroLOTTO пользователя Флексей Шишкинс',
                    'payment_num' => 'VWANmkmUY2Yo82a0Oi7jiIMAfMrXEPSb',
                    'sucess_url' => 'http://euro-lotto.club/payment/fchange-success',
                    'error_url' => 'http://euro-lotto.club/payment/fchange-failure',
                    'obmen_order_id' => 3281,
                    'obmen_recive_valute' => 'usd',
                    'obmen_timestamp' => 1483727749,
                    'verificate_hash' => 'd46adb1d5676ef5840a44fd8d3805a58241abdbfb4312a7356a2995e986fd0e7',
                ])
                ->setOptions([
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                ])
                ->send();
        if ($response->isOk) {
            echo 'OK';
        }
        echo \yii\helpers\VarDumper::dumpAsString($response->content);
    }
    public function actionTestPost2()
    {

        $client   = new Client(
                [
//            'requestConfig' => [
//            ],
            'transport' => 'yii\httpclient\CurlTransport',
//            'transport'     => 'yii\httpclient\StreamTransport',
                ]
        );
        $response = $client->createRequest()
                ->setMethod('post')
//                ->setUrl('http://euro-lotto.club/payment/fchange-status')
//                ->setUrl('http://frontend.lottery/payment/fchange-status')
                ->setUrl('https://simple.freedom-lotto.com/payment/fchange-status')
                ->setData([
                    'merchant_name' => 'yandexmoney2',
                    'merchant_title' => 'FreedomLOTTO',
                    'payed_paysys' => 'QWRUB',
                    'amount' => 0.01,
                    'payment_info' => 'Пополнение счёта FreedomLOTTO пользователя test.fchange',
                    'payment_num' => 'eIn71a-WQAJAbSJb8jHOgOad9SrK0kNb',
                    'sucess_url' => 'http://freedom-lotto.com/payment/fchange-success',
                    'error_url' => 'http://freedom-lotto.com/payment/fchange-failure',
                    'obmen_order_id' => 5490,
                    'obmen_recive_valute' => 'usd',
                    'obmen_timestamp' => 1484681766,
                    'verificate_hash' => '5aa6dbb3ff43dfe1ebc25133a5e77842c4a4c87cd327f2746b56766686bc8b4f',
                ])
                ->setOptions([
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                ])
                ->send();
        if ($response->isOk) {
            echo 'OK';
        }
        echo \yii\helpers\VarDumper::dumpAsString($response->content);
    }

}
