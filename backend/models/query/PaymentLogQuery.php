<?php

namespace backend\models\query;

//use backend\models\SystemLog;
use yii\db\ActiveQuery;

/**
 * PaymentLogQuery
 *
 * @author Mega
 */
class PaymentLogQuery extends ActiveQuery
{

    public function next($current_id)
    {
        return $this->andWhere(['>', 'id', $current_id])
                        ->orderBy('id DESC')
                        ->one();
    }

    public function prev($current_id)
    {
        return $this->andWhere(['<', 'id', $current_id])
                        ->orderBy('id DESC')
                        ->one();
    }

}
