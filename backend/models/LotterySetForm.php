<?php

namespace backend\models;

use yii\base\Model;
use Yii;

/**
 * Create user form
 */
class LotterySetForm extends Model
{

    public $count;
    public $firstDrawAt;
    public $period;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'period'], 'integer', 'min' => 1],
            [['count', 'period', 'firstDrawAt'], 'required'],
            ['firstDrawAt', 'default', 'value' => null],
            ['firstDrawAt', 'date', 'timestampAttribute' => 'firstDrawAt', 'format'=>'php:d-m-Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'count'       => Yii::t('common', 'Count of Draws'),
            'period'      => Yii::t('common', 'Period'),
            'firstDrawAt' => Yii::t('common', 'First Draw at'),
        ];
    }

}
