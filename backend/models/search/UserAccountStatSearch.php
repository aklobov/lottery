<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserAccountStat;
use cheatsheet\Time;

/**
 * UserAccountStatSearch represents the model behind the search form about `common\models\UserAccountStat`.
 */
class UserAccountStatSearch extends UserAccountStat
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'updated_at'], 'integer'],
            [['direction', 'system', 'status', 'target', 'operation_id', 'description'], 'safe'],
            [['amount'], 'number'],
            ['created_at', 'default', 'value' => null],
            ['created_at', 'date', 'timestampAttribute' => 'created_at', 'format' => 'php:d-m-Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserAccountStat::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'updated_at' => $this->updated_at,
        ]);

//        print_r($this->created_at);
//        die();
        $query->andFilterWhere(['like', 'direction', $this->direction])
                ->andFilterWhere(['like', 'system', $this->system])
                ->andFilterWhere(['like', 'status', $this->status])
                ->andFilterWhere(['like', 'target', $this->target])
                ->andFilterWhere(['like', 'operation_id', $this->operation_id])
                ->andFilterWhere(['like', 'description', $this->description]);
        if (!empty($this->created_at)) {
            $this->created_at += Time::SECONDS_IN_A_DAY / 2;
            $query->andFilterWhere(['<', "ABS({$this->created_at} - created_at)", Time::SECONDS_IN_A_DAY / 2]);
        }

        return $dataProvider;
    }

}
