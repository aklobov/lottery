<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AccountLog;

/**
 * SystemLogSearch represents the model behind the search form about `backend\models\SystemLog`.
 */
class AccountLogSearch extends AccountLog
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'log_time', 'level'], 'integer'],
            [['category', 'prefix'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AccountLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'log_time' => SORT_DESC,
                ]
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
//            'id' => $this->id,
            'level' => $this->level,
//            'log_time' => $this->log_time,
//            'message' => $this->message,
//            'prefix' => $this->prefix,
        ]);

        if (isset($this->category)) {
            $query->andFilterWhere(['like', 'category', $this->category]);
        }
        if (isset($this->prefix)) {
            $query->andFilterWhere(['like', 'prefix', "[{$this->prefix}]"]);
        }
//        $qq = $query->createCommand()->getRawSql();
        return $dataProvider;
    }

}
