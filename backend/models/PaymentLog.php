<?php

namespace backend\models;

use backend\models\query\PaymentLogQuery;

class PaymentLog extends SystemLog
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_log}}';
    }

        public static function find()
    {
        return new PaymentLogQuery(get_called_class());
    }

}
