<?php

namespace backend\models;

//namespace common\models;
//
use backend\models\query\AccountLogQuery;

class AccountLog extends SystemLog
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account_log}}';
    }
    
        public static function find()
    {
        return new AccountLogQuery(get_called_class());
    }

}
