<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\lottery\models\instant1x3Ticket;

/**
 * instant1x3TicketSearch represents the model behind the search form about `common\components\lottery\models\instant1x3Ticket`.
 */
class instant1x3TicketSearch extends instant1x3Ticket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'paid', 'win_combination', 'win_cnt', 'bet', 'created_at', 'updated_at'], 'integer'],
            [['paid_out'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = instant1x3Ticket::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'paid' => $this->paid,
            'win_combination' => $this->win_combination,
            'win_cnt' => $this->win_cnt,
            'paid_out' => $this->paid_out,
            'bet' => $this->bet,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
