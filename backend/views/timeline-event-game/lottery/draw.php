<?php
/**
 * @author Alexander K <al@gmail.com>
 * @var $model common\models\TimelineEventGame
 */
?>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>

    <h3 class="timeline-header">
        <?php echo Yii::t('backend', 'Lottery drawed!') ?>
    </h3>

    <div class="timeline-body">
        <?php
        echo Yii::t('backend', 'Lottery <strong>{type}</strong> [draw № {identity}] was drawed at <strong>{draw_at}</strong>', [
            'identity' => $model->data['id'],
            'type' => Yii::t('backend', $model->data['type']),
            'draw_at' => Yii::$app->formatter->asDatetime($model->data['draw_at']),
        ])
        ?>
    </div>

    <div class="timeline-footer">
        <?php
        echo \yii\helpers\Html::a(
                Yii::t('backend', 'View lottery'), ["/l{$model->data['type']}/view", 'id' => $model->data['id']],
//                    ['index'],
                ['class' => 'btn btn-info btn-sm']
        );
        printf(' Win combination is [%s].', implode(', ',$model->data['draw']));
        ?>
    </div>
</div>