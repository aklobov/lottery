<?php

/**
 * @author Alexander K <al@gmail.com>
 * @var $model common\models\TimelineEventGame
 */
use common\components\lottery\models\L6x45Ticket;
use common\components\lottery\models\L1x3Ticket;
use common\components\lottery\models\L3x9Ticket;
?>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>

    <h3 class="timeline-header">
        <?php echo Yii::t('backend', 'New bet created!') ?>
    </h3>

    <div class="timeline-body">
        <?php
        echo Yii::t('backend', 'New bet <strong>{type}</strong> [draw № {identity}] was registered at {created_at} by user [{user_id}]', [
            'identity' => $model->data['id'],
            'type' => $model->data['type'],
            'user_id' => $model->data['user_id'],
            'created_at' => Yii::$app->formatter->asDatetime($model->data['created_at']),
        ])
        ?>
    </div>

    <div class="timeline-footer">
        <?php
        if (in_array($model->data['lottery_type_id'], [
                    L6x45Ticket::ID,
                ])) {


            echo \yii\helpers\Html::a(
                    Yii::t('backend', 'View bet'), '@frontendUrl/lottery' . $model->data['lottery_type_id'] . '/check-ticket?ticket_id=' . $model->data['id'], [
                'class' => 'btn btn-success btn-sm',
                'target' => '_blank',
//            'data'   => [
//                'method' => 'post',
//                'params' => [
//                    'LCheckTicketForm' => [
//                        'ticket_id' => $model->data['id'],
//                    ]],
//            ]
                    ]
            );
        } else {
            //Instant Lotteries (1x3, 3x9)
            $class  = 'common\components\lottery\models\L' . $model->data['lottery_type_id'] . 'Ticket';
            $ticket = $class::findOne($model->data['id']);
            if (!empty($ticket)) {
                $message = sprintf(Yii::t('backend', 'Bet: [%s]. Paid: [%s]. Win numbers: [%d]. Win combination: [%s]. Win: [%s]'), $model->data['numbers'], $ticket->paidReadable, $ticket->win_cnt, $ticket->win_combinationReadable, $ticket->paid_out_Readable);
            } else {
                $message =  '<p>Нет данных.</p>';
            }
            echo "<p class='text-primary'>$message</p>";
        }
        ?>
    </div>
</div>