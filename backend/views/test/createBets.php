<?php

//FIXME Remove view in production
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = 'Create Bets';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Lotteries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        Created Bets
    </p>
    <ul>
        <?php
        foreach ($model as $item) {
            $dt = Yii::$app->formatter->asDatetime($item->created_at);
            echo "<li>Bet №$item->id was set at $dt.</li>";

        }
        ?>
    </ul>
    <pre><?= print_r($model, true) ?></pre>

</div>
