<?php
//FIXME Remove view in production
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = 'Test page';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        Test
    </p>
    <?php date_default_timezone_set('Europe/Moscow'); ?>
    time: <?=time() ?><br />
    mktime: <?=mktime(0) ?> <br />
    delta: <?=(mktime(12)-time())/3600 ?> <br />
    date(h): <?= date('H') ?>
    <pre><?=print_r(getdate(\common\components\lottery\l5x36\Lottery::ceilToQuarterHour(time())),true)?></pre>
    <pre><?=print_r($model,true)?></pre>

</div>
