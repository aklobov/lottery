<?php

//FIXME Remove view in production
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = '6 out of 45 Draw Summary';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Test functions')];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-view">
    <h1><?= $model->name ?></h1>
    <p class="lead">Draw №<?= $model->id ?> [<?= Yii::$app->formatter->asDate($model->draw_at) ?>]</p>
    <pre>
        <?= $model->wins_stat ?>
    </pre>
</div>
