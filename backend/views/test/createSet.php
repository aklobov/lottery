<?php

//FIXME Remove view in production
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = 'Create Set';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Lotteries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        Created lotteries
    </p>
    <ul>
        <?php
        foreach ($model as $lottery) {
            $dt = Yii::$app->formatter->asDatetime($lottery->draw_at);
            echo "<li>Lottery №$lottery->id draw at $dt.</li>";

        }
        ?>
    </ul>
    <pre><?= print_r([], true) ?></pre>

</div>
