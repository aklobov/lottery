<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserAccountStat */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'User Account Stat',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'User Account Stats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-account-stat-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
