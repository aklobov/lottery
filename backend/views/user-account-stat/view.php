<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserAccountStat */

$this->title                   = $model->user->publicIdentity;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'User Account Stats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-account-stat-view">


    <?php
    echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
//            'user_id',
            [
                'attribute' => 'user_id',
                'format'    => 'raw',
                'value'     => $model->user->publicIdentity . ' ' . Html::a("[$model->user_id]", ['/user/view', 'id' => $model->user_id]),
            ],
            'direction',
            'amount',
            'system',
            'status',
            'target',
            'operation_id',
            'description',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>

</div>
