<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\UserAccountStat;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserAccountStatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Экспорт';
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Payment Control'), 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-stat-export-index">
    <div class="pull-left">
        <?= Html::a(Yii::t('backend', 'Back to menu') . ' ' . Yii::t('backend', 'Payment Control'), 'index', ['role' => 'button', 'class' => 'btn btn-default']) ?>
    </div>
    <div class="pull-right">    
        <?php
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'user_id',
                [
                    'label' => 'Пользователь',
                    'attribute' => 'user_id',
                    'content' => function($model, $key) {
                        $content = $model->user->publicIdentity . Html::a(" [$model->user_id]", ['/user/view', 'id' => $model->user_id]);
                        return $content;
                    }
                        ],
                        'direction',
                        'status',
                        'amount',
                        'system',
                        'target',
                        'operation_id',
                        'created_at:datetime',
                        'updated_at:datetime',
//                'description',
                    ],
                    'dropdownOptions' => [
                        'label' => 'Экспортировать',
                        'class' => 'btn btn-default',
//                        'title' => ' ',
                    ],
                    'target' => ExportMenu::TARGET_SELF,
                    'exportConfig' => [
                        ExportMenu::FORMAT_EXCEL => false,
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_TEXT => false,
                    ],
                    'filename' => 'Export_'.date('Y_m_d-H_i'),
                    'showConfirmAlert' => false,
                ]);
                ?>
            </div>
            <div class="clearfix"></div>
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
                'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
                    'id',
//            'user_id' ,
                    [
                        'label' => 'Пользователь',
                        'attribute' => 'user_id',
                        'content' => function($model, $key) {
                            $content = $model->user->publicIdentity . Html::a(" [$model->user_id]", ['/user/view', 'id' => $model->user_id]);
                            return $content;
                        }
                            ],
                            [
                                'class' => 'common\grid\EnumColumn',
                                'attribute' => 'direction',
                                'enum' => ['in' => 'Ввод', 'out' => 'Вывод'],
                            ],
                            'amount',
                            'system',
                            [
                                'class' => 'common\grid\EnumColumn',
                                'attribute' => 'status',
                                'enum' => UserAccountStat::getStatuses(),
                                'content' => function($model, $key) {
                                    $res = $model->status;
                                    if ($model->status == UserAccountStat::STATUS_POSTPONED) {
                                        $res = Html::tag('span', $model->status, ['class' => 'label label-warning']);
                                    } elseif (in_array($model->status, [
                                                UserAccountStat::STATUS_CANCELLED,
                                                UserAccountStat::STATUS_CANCELLED_BY_USER,
                                                UserAccountStat::STATUS_FINISHED,
                                                UserAccountStat::STATUS_CONFIRMED,
                                                UserAccountStat::STATUS_APPROVED,
                                            ])) {
                                        $res = Html::tag('span', $model->status, ['class' => 'label label-info']);
                                    } else {
                                        $res = Html::tag('span', $model->status, ['class' => 'label label-default']);
                                    }
                                    return $res;
                                },
                                    ],
                                    // 'target',
                                    // 'operation_id',
                                    // 'description',
                                    'created_at:datetime',
                                    // 'updated_at',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{view}',
                                    ],
                                ],
                            ]);

                            echo Html::endForm();
                            ?>

</div>
