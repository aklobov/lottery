<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use common\models\UserAccountStat;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserAccountStatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('backend', 'Payment Control');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-account-stat-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::beginForm(['user-account-stat/index'], 'post'); ?>

    <?= Html::dropDownList('action', '', ['' => Yii::t('backend', 'Изменить выбранные:'), UserAccountStat::STATUS_APPROVED => Yii::t('backend', 'Выполнить перевод'), UserAccountStat::STATUS_CANCELLED => Yii::t('backend', 'Отменить перевод')], ['class' => 'dropdown',]) ?>

    <?= Html::submitButton(Yii::t('backend', 'Set'), ['class' => 'btn btn-info',]); ?>

    <?php
    $isFa                          = true;
    $exportConfig                  = [
        GridView::CSV => ['label' => 'Save as CSV'],
        GridView::HTML => [
            'label' => Yii::t('kvgrid', 'HTML'),
            'icon' => $isFa ? 'file-text' : 'floppy-saved',
            'iconOptions' => ['class' => 'text-info'],
            'showHeader' => true,
            'showPageSummary' => true,
            'showFooter' => true,
            'showCaption' => true,
            'filename' => Yii::t('kvgrid', 'grid-export'),
            'alertMsg' => Yii::t('kvgrid', 'The HTML export file will be generated for download.'),
            'options' => ['title' => Yii::t('kvgrid', 'Hyper Text Markup Language')],
            'mime' => 'text/html',
            'config' => [
                'cssFile' => 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'
            ]
        ],
    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'toolbar' => [
            '{export}', '{toogleData}'
        ],
        'export' => [
            'fontAwesome' => $isFa,
            'label' => 'Export',
        ],
        'exportConfig' => $exportConfig,
//                                        'panel' => [
//                                            'type' => GridView::TYPE_PRIMARY,
//                                            'heading' => $heading,
//                                        ],
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'headerOptions' => ['class' => 'text-center', 'style' => 'width: 2em'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            'id',
//            'user_id' ,
            [
                'label' => 'Пользователь',
                'attribute' => 'user_id',
                'content' => function($model, $key) {
                    $content = $model->user->publicIdentity . Html::a(" [$model->user_id]", ['/user/view', 'id' => $model->user_id]);
                    return $content;
                }
                    ],
                    [
                        'class' => 'common\grid\EnumColumn',
                        'attribute' => 'direction',
                        'enum' => ['in' => 'Ввод', 'out' => 'Вывод'],
                    ],
                    'amount',
                    'system',
                    [
                        'class' => 'common\grid\EnumColumn',
                        'attribute' => 'status',
                        'enum' => UserAccountStat::getStatuses(),
                        'content' => function($model, $key) {
                            $res = $model->status;
                            if ($model->status == UserAccountStat::STATUS_POSTPONED) {
                                $res = Html::tag('span', $model->status, ['class' => 'label label-warning']);
                            } elseif (in_array($model->status, [
                                        UserAccountStat::STATUS_CANCELLED,
                                        UserAccountStat::STATUS_CANCELLED_BY_USER,
                                        UserAccountStat::STATUS_FINISHED,
                                        UserAccountStat::STATUS_CONFIRMED,
                                        UserAccountStat::STATUS_APPROVED,
                                    ])) {
                                $res = Html::tag('span', $model->status, ['class' => 'label label-info']);
                            } else {
                                $res = Html::tag('span', $model->status, ['class' => 'label label-default']);
                            }
                            return $res;
                        },
                            ],
                            // 'target',
                            // 'operation_id',
                            // 'description',
                            'created_at:datetime',
                            // 'updated_at',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view}',
                            ],
                        ],
                    ]);

                    echo Html::endForm();
                    ?>

    <div class="panel panel-default">
        <h3>Статус операции</h3>
        <dl class="dl-horizontal">
            <dt>requested</dt>
            <dd>Новая заявка на вывод денежных средств. Сумма денежных средств превышает пороговое значение, когда перевод выполняется в автоматическом режиме. Заявка ожидает подтверждения от пользователя.</dd>
            <dt>finished</dt>
            <dd>Сумма денежных средств не превышает пороговых значений . Перевод выполнен в автоматическом режиме.</dd>
            <!--            <dt>confirmed</dt>
                        <dd>Сумма денежных средств превышает пороговое значение, когда перевод выполняется в автоматическом режиме. Пользователь подтвердил запрос на вывод средств.</dd>-->
            <dt>postponed</dt>
            <dd>Заявка на вывод денежных средств ожидает решения оператора.</dd>
            <dt>approved</dt>
            <dd>Заявка на вывод денежных средств подтверждена оператором и перевод выполнен.</dd>
            <dt>error</dt>
            <dd>Заявка на вывод денежных средств подтверждена оператором и перевод не выполнен.</dd>
            <dt>cancelled</dt>
            <dd>Заявка на вывод денежных средств отклонена оператором.</dd>
            <dt>cancelled by user</dt>
            <dd>Заявка на вывод денежных средств отменена пользователем.</dd>
        </dl>
        <p class="text-danger">Важно! Все операции нужно доводить до финальных состояний (finished, confirmed, approved, cancelled, cancelled by user). Только при этих состояниях платеж проводится или корректно отменяется (деньги возвращаются пользователю).</p>
    </div>
</div>
