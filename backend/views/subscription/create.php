<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionModel */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Subscription Model',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Subscription Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-model-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
