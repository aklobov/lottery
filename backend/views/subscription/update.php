<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubscriptionModel */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Subscription Model',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Subscription Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="subscription-model-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
