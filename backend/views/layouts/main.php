<?php

/**
 * @var $this yii\web\View
 */
use kartik\alert\AlertBlock;
use kartik\growl\Growl;
?>
<?php $this->beginContent('@backend/views/layouts/common.php'); ?>
<?php
$config = [
//            'title'         => '<h3>Upgrade your account</h3>',
    'showSeparator' => true,
//                    'icon'          => 'glyphicon glyphicon-ok-sign',
    'pluginOptions' => [
        'showProgressbar' => true,
        'delay'           => Yii::$app->params['growlNotificationShowTime'],
        'mouse_over'      => 'pause',
    ]
];
AlertBlock::widget([
    'useSessionFlash' => true,
    'type'            => AlertBlock::TYPE_GROWL,
    'delay'           => 1000,
    'alertSettings'   => [
        'info'    => ['type' => Growl::TYPE_INFO] + $config,
        'success' => ['type' => Growl::TYPE_SUCCESS] + $config,
        'warning' => ['type' => Growl::TYPE_WARNING] + $config,
        'error'   => ['type' => Growl::TYPE_DANGER] + $config,
    ],
]);
?>
<div class="box">
    <div class="box-body">
        <?php echo $content ?>
    </div>
</div>
<?php $this->endContent(); ?>