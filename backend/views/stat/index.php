<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('backend', 'Statistics');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("window.paceOptions = { ajax: false }", \yii\web\View::POS_HEAD);
$this->registerJsFile(
        Yii::$app->request->baseUrl . 'js/system-information/index.js', ['depends' => ['\yii\web\JqueryAsset', '\common\assets\Flot', '\yii\bootstrap\BootstrapPluginAsset']]
)
?>
<div class="stat-index">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Reload'), '', ['class' => 'btn btn-info']) ?>
    </p>

    <div class="row connectedSortable">
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_4.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '1 out of 3') ?><br><span class="badge">24 часа</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l1x3Stat['day']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l1x3Stat['day']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l1x3Stat['day']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_4.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '1 out of 3') ?><br><span class="badge">7 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l1x3Stat['week']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l1x3Stat['week']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l1x3Stat['week']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_4.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '1 out of 3') ?><br><span class="badge">30 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l1x3Stat['month']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l1x3Stat['month']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l1x3Stat['month']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_5.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '3 out of 9') ?><br><span class="badge">24 часа</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l3x9Stat['day']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l3x9Stat['day']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l3x9Stat['day']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_5.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '3 out of 9') ?><br><span class="badge">7 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l3x9Stat['week']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l3x9Stat['week']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l3x9Stat['week']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_5.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '3 out of 9') ?><br><span class="badge">30 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l3x9Stat['month']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l3x9Stat['month']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l3x9Stat['month']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_6.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Slots Five') ?><br><span class="badge">24 часа</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $slotsFiveStat['day']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($slotsFiveStat['day']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($slotsFiveStat['day']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_6.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Slots Five') ?><br><span class="badge">7 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $slotsFiveStat['week']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($slotsFiveStat['week']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($slotsFiveStat['week']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_6.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Slots Five') ?><br><span class="badge">30 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $slotsFiveStat['month']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($slotsFiveStat['month']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($slotsFiveStat['month']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_2.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '6 out of 45') ?><br><span class="badge">24 часа</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l6x45Stat['day']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l6x45Stat['day']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l6x45Stat['day']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_2.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '6 out of 45') ?><br><span class="badge">7 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l6x45Stat['week']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l6x45Stat['week']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l6x45Stat['week']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/lotto_2.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', '6 out of 45') ?><br><span class="badge">30 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Куплено билетов') ?></dt>
                        <dd><?php echo $l6x45Stat['month']['bets_count'] ?></dd>

                        <dt><?php echo Yii::t('backend', 'Оплачено за билеты') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l6x45Stat['month']['bets_paid']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Выплачено выигрыша') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($l6x45Stat['month']['bets_paid_out']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <!-- Внешние операции -->
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/perfectmoney.png', ['style' => 'max-width: 167px;']) ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Perfect Money') ?><br><span class="badge">24 часа</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['perfectmoney']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['perfectmoney']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['perfectmoney']['payment'] - $payStat['day']['perfectmoney']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/perfectmoney.png', ['style' => 'max-width: 167px;']) ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Perfect Money') ?><br><span class="badge">7 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['perfectmoney']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['perfectmoney']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['perfectmoney']['payment'] - $payStat['week']['perfectmoney']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/perfectmoney.png', ['style' => 'max-width: 167px;']) ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Perfect Money') ?><br><span class="badge">30 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['perfectmoney']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['perfectmoney']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['perfectmoney']['payment'] - $payStat['month']['perfectmoney']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <!-- Payeer -->
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/payeer.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Payeer') ?><br><span class="badge">24 часа</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['payeer']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['payeer']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['payeer']['payment'] - $payStat['day']['payeer']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/payeer.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Payeer') ?><br><span class="badge">7 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['payeer']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['payeer']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['payeer']['payment'] - $payStat['week']['payeer']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/payeer.png') ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'Payeer') ?><br><span class="badge">30 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['payeer']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['payeer']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['payeer']['payment'] - $payStat['month']['payeer']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <!-- F-Change -->
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/fchange.png', ['style' => 'max-width: 167px;']) ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'F-CHANGE') ?><br><span class="badge">24 часа</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['fchange']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['fchange']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['day']['fchange']['payment'] - $payStat['day']['fchange']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/fchange.png', ['style' => 'max-width: 167px;']) ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'F-CHANGE') ?><br><span class="badge">7 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['fchange']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['fchange']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['week']['fchange']['payment'] - $payStat['week']['fchange']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="box box-primary">
                <div class="box-header">
                    <?= Html::img('@frontendUrl/img/fchange.png', ['style' => 'max-width: 167px;']) ?>
                    <h3 class="box-title"><?php echo Yii::t('backend', 'F-CHANGE') ?><br><span class="badge">30 дней</span></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt><?php echo Yii::t('backend', 'Ввод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['fchange']['payment']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Вывод') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['fchange']['payout']) ?></dd>

                        <dt><?php echo Yii::t('backend', 'Баланс (Ввод - Вывод)') ?></dt>
                        <dd><?php echo Yii::$app->formatter->asCurrency($payStat['month']['fchange']['payment'] - $payStat['month']['fchange']['payout']) ?></dd>
                    </dl>
                </div><!-- /.box-body -->
            </div>
        </div>

    </div>
</div>
