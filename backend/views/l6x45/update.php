<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\L6x45 */

$this->title = 'Update 6 out of 45 lottery №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => '6 out of 45', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Lottery №'.$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="l6x45-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
