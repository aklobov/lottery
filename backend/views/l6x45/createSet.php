<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\L6x45 */

$this->title                   = 'Create set of 6 out of 45 lotteries';
$this->params['breadcrumbs'][] = ['label' => '6 out of 45', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="l6x45-create-set">
    <?php
    $form                          = ActiveForm::begin();
    echo $form->errorSummary($model);
    echo $form->field($model, 'count')->dropdownList(array_combine(range(1,20),range(1,20)));
    echo $form->field($model, 'period')->dropdownList(array_combine(range(1,10),range(1,10)))->hint('Period, days');
    echo $form->field($model, 'firstDrawAt')->widget(DatePicker::className(), ['dateFormat' => 'php:d-m-Y', 'clientOptions' => ['changeMonth' => 1, 'changeYear' => 1, 'showWeek' => 1, 'minDate'=>0]]);
    ?>
    <div class="form-group">
        <?php echo Html::submitButton('Create Set', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
