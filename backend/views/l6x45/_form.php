<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\L6x45 */
/* @var $form yii\bootstrap\ActiveForm */

$disableOutdated = empty($model->id)?['minDate'=>0]:[];
?>

<div class="l6x45-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php // echo $form->field($model, 'id')->textInput() ?>

    <?php // echo $form->field($model, 'draw')->textInput() ?>

    <?php echo $form->field($model, 'draw_at')->widget(DatePicker::className(), ['dateFormat' => 'php:d-m-Y', 'clientOptions' => ['changeMonth' => 1, 'changeYear' => 1, 'showWeek' => 1] + $disableOutdated]) ?>

    <?php echo $form->field($model, 'superprize')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'superprize_gain')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'tickets')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'bets')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'pool')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'paid_out')->textInput(['maxlength' => true]) ?>

    <?php // echo $form->field($model, 'wins_stat')->textInput() ?>

    <?php echo $form->field($model, 'enabled')->checkbox() ?>

    <?php // echo $form->field($model, 'created_at')->textInput() ?>

    <?php // echo $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
