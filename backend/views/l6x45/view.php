<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\L6x45 */

$this->title = $model->getName() . ' Lottery [Draw №' . $model->id.'] ('.Yii::$app->formatter->asDatetime($model->draw_at).')';
$this->params['breadcrumbs'][] = ['label' => '6 out of 45', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="l6x45-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        echo Html::a($model->enabled ? 'Disable' : 'Enable', ['toggle', 'id' => $model->id, 'view' => 'view'], [
            'class' => $model->enabled ? 'btn btn-danger' : 'btn btn-success',
            'data' => [
//                'confirm' => $model->enabled ? 'Are you sure you want to disable this lottery?' : 'Are you sure you want to enable this lottery?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'enabled',
            'draw',
            'draw_at:datetime',
            'superprize:currency',
            'superprize_gain:currency',
            'tickets',
            'bets',
            'pool',
            'paid_out',
            'wins_stat',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ])
    ?>

</div>
