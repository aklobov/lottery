<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\L6x45Search */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="l6x45-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'draw') ?>

    <?php echo $form->field($model, 'draw_at') ?>

    <?php echo $form->field($model, 'superprize') ?>

    <?php echo $form->field($model, 'superprize_gain') ?>

    <?php // echo $form->field($model, 'tickets') ?>

    <?php // echo $form->field($model, 'bets') ?>

    <?php // echo $form->field($model, 'pool') ?>

    <?php // echo $form->field($model, 'paid_out') ?>

    <?php // echo $form->field($model, 'wins_stat') ?>

    <?php echo $form->field($model, 'enabled') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
