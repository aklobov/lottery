<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\L6x45 */

$this->title = 'Create 6 out of 45 lottery';
$this->params['breadcrumbs'][] = ['label' => '6 out of 45', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="l6x45-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
