<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\components\lottery\models\L6x45Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '6 out of 45';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="l6x45-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Lottery', ['create'], ['class' => 'btn btn-success']) ?>
        <?php echo Html::a('Create Lottery Set', ['create-set'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
//            'draw_at:date',
            [
                'attribute' => 'draw_at',
                'value' => 'draw_at',
                'format' => 'date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'draw_at',
                    'dateFormat' => 'php:d-m-Y',
                    'clientOptions' => ['changeMonth' => 1, 'changeYear' => 1, 'showWeek' => 1]
                    ]),
            ],
            'draw',
            'superprize:currency',
            'superprize_gain:currency',
            // 'bets',
            'pool',
            'paid_out',
            'tickets',
            [
                'attribute' => 'enabled',
                'format' => 'boolean',
                'filter' => [1 => 'Yes', 0 => 'No'],
            ],
            // 'wins_stat',
            // 'created_at',
            // 'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view} {update} {toggle}',
                'buttons' => [
                    'toggle' => function($url, $model) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-off"></span>', $url, [
                                    'title' => $model->enabled ? 'Disable' : 'Enable',
                                    'data-pjax' => '0',
                        ]);
                    }]
                    ],
                ],
            ]);
            ?>

</div>
