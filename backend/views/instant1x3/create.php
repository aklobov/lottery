<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\instant1x3Ticket */

$this->title = Yii::t('frontend', 'Create Instant1x3 Ticket');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Instant1x3 Tickets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instant1x3-ticket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
