<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\components\lottery\models\instant1x3Ticket */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Instant1x3 Ticket',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Instant1x3 Tickets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="instant1x3-ticket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
