<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\log\Logger;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\AccountLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('backend', 'Account Logs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-log-index">

    <p>
        <?php //echo Html::a(Yii::t('backend', 'Clear'), false, ['class' => 'btn btn-danger', 'data-method' => 'delete']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'class'     => 'common\grid\EnumColumn',
                'attribute' => 'level',
                'enum'      => [
                    Logger::LEVEL_ERROR   => Logger::getLevelName(Logger::LEVEL_ERROR),
                    Logger::LEVEL_WARNING => Logger::getLevelName(Logger::LEVEL_WARNING),
                    Logger::LEVEL_INFO    => Logger::getLevelName(Logger::LEVEL_INFO),
                    Logger::LEVEL_TRACE   => Logger::getLevelName(Logger::LEVEL_TRACE),
                ],
            ],
//            [
//                'attribute' => 'level',
//                'value' => function ($model) {
//                    return \yii\log\Logger::getLevelName($model->level);
//                },
//                'filter' => [
//                    \yii\log\Logger::LEVEL_ERROR => 'error',
//                    \yii\log\Logger::LEVEL_WARNING => 'warning',
//                    \yii\log\Logger::LEVEL_INFO => 'info',
//                    \yii\log\Logger::LEVEL_TRACE => 'trace',
//                ]
//            ],
            'category',
            'prefix:text:Id пользователя',
            [
                'attribute' => 'log_time',
                'format'    => 'datetime',
                'value'     => function ($model) {
                    return (int) $model->log_time;
                }
            ],
            [
                'attribute' => 'message',
                'format'    => 'text',
                'header'    => 'Списано/начислено',
                'value'     => function ($model) {
                    preg_match_all('/\[([\d\.]+)\]/', $model->message, $matches);
//                    return print_r($matches,1);
                    return isset($matches[1][1]) ? $matches[1][1] : print_r($matches, 1);
                }
            ],
            [
                'attribute' => 'message',
                'format'    => 'text',
                'header'    => 'Счёт',
                'value'     => function ($model) {
                    preg_match_all('/\[([\d\.]+)\]/', $model->message, $matches);
//                    return print_r($matches,1);
                    return isset($matches[1][2]) ? $matches[1][2] : print_r($matches, 1);
                }
            ],
            [
                'attribute' => 'message',
                'format'    => 'text',
                'header'    => 'Событие',
                'value'     => function ($model) {
                    preg_match_all('/\[([^\[]+)\]/', $model->message, $matches);
//                    return print_r($matches,1);
                    return isset($matches[1][3]) ? $matches[1][3] : print_r($matches, 1);
                }
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}'
            ]
        ]
    ]);
    ?>

</div>
