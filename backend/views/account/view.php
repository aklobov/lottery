<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AccountLog */

$this->title                   = Yii::t('backend', 'Record #{id}', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Account Logs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$class = $model->className();
$next  = $class::find()->next($model->id);
$prev  = $class::find()->prev($model->id);
?>
<div class="account-log-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger', 'data' => ['method' => 'post']]) ?>
        <?php echo Html::a(Yii::t('backend', 'Next'), ['view', 'id' => isset($next->id) ? $next->id : $model->id], ['class' => 'btn btn-success pull-right' . (empty($next) ? ' disabled' : '')]) ?>
        <span class="pull-right">&nbsp;</span>
        <?php echo Html::a(Yii::t('backend', 'Previous'), ['view', 'id' => isset($prev->id) ? $prev->id : $model->id], ['class' => 'btn btn-success pull-right' . (empty($prev) ? ' disabled' : '')]) ?>
    </p>

    <?php
    echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'level',
                'format'    => 'raw',
                'value'     => '<span class="badge">' . \yii\log\Logger::getLevelName($model->level) . '</span>',
            ],
            'category',
            [
                'attribute' => 'log_time',
                'format'    => 'datetime',
                'value'     => (int) $model->log_time
            ],
            'prefix:ntext:Id пользователя',
            [
                'attribute' => 'message',
                'format'    => 'raw',
                'value'     => Html::tag('pre', $model->message, ['style' => 'white-space: pre-wrap'])
            ],
        ],
    ])
    ?>

</div>
