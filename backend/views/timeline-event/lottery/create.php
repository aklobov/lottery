<?php
/**
 * @author Alexander K <al@gmail.com>
 * @var $model common\models\TimelineEvent
 */
?>
<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>

    <h3 class="timeline-header">
        <?php echo Yii::t('backend', 'New lottery created!') ?>
    </h3>

    <div class="timeline-body">
        <?php
        echo Yii::t('backend', 'New lottery <strong>{type}</strong> [draw № {identity}] was registered at {created_at} and will be drawed at <strong>{draw_at}</strong>', [
            'identity'   => $model->data['id'],
            'type'       => $model->data['type'],
            'created_at' => Yii::$app->formatter->asDatetime($model->data['created_at']),
            'draw_at'    => Yii::$app->formatter->asDatetime($model->data['draw_at']),
        ])
        ?>
    </div>

    <div class="timeline-footer">
        <?php
        echo \yii\helpers\Html::a(
                Yii::t('backend', 'View lottery'), ["/l{$model->data['type']}/view", 'id' => $model->data['id']],
//                    ['index'],
                       ['class' => 'btn btn-success btn-sm']
        )
        ?>
    </div>
</div>