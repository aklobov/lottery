<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\log\Logger;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('backend', 'Payment Logs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-log-index">

    <p>
        <?php //echo Html::a(Yii::t('backend', 'Clear'), false, ['class' => 'btn btn-danger', 'data-method' => 'delete']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class'     => 'common\grid\EnumColumn',
                'attribute' => 'level',
                'enum'      => [
                    Logger::LEVEL_ERROR   => Logger::getLevelName(Logger::LEVEL_ERROR),
                    Logger::LEVEL_WARNING => Logger::getLevelName(Logger::LEVEL_WARNING),
                    Logger::LEVEL_INFO    => Logger::getLevelName(Logger::LEVEL_INFO),
                    Logger::LEVEL_TRACE   => Logger::getLevelName(Logger::LEVEL_TRACE),
                ],
            ],
//            [
//                'attribute' => 'level',
//                'value'     => function ($model) {
//                    return \yii\log\Logger::getLevelName($model->level);
//                },
//                'filter' => [
//                    \yii\log\Logger::LEVEL_ERROR   => 'error',
//                    \yii\log\Logger::LEVEL_WARNING => 'warning',
//                    \yii\log\Logger::LEVEL_INFO    => 'info',
//                    \yii\log\Logger::LEVEL_TRACE   => 'trace',
//                ]
//            ],
            'category',
            'prefix:text:Id пользователя',
            [
                'attribute' => 'log_time',
                'format'    => 'datetime',
                'value'     => function ($model) {
                    return (int) $model->log_time;
                }
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}'
            ]
        ]
    ]);
    ?>

</div>
