<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\grid\EnumColumn;
use common\models\StopList;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StopListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Stop List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="black-list-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        echo Html::a(Yii::t('backend', 'Create', [
                    'modelClass' => 'Stop List',
                ]), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'type',
            'value',
            'is_regexp:boolean',
//            [
//                'class' => EnumColumn::className(),
//                'attribute' => 'is_regexp',
//                'enum' => StopList::$types,
//                'filter' => StopList::$types,
//            ],
            'created_at:datetime',
            'updated_at:datetime',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
