<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StopList */

$this->title = Yii::t('backend', 'Create item', [
    'modelClass' => 'Stop List',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Stop List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="black-list-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
