<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\StopList;

/* @var $this yii\web\View */
/* @var $model common\models\StopList */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="black-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php // echo $form->field($model, 'type')->textInput(['maxlength' => true]) ?>
    <?php echo $form->field($model, 'type')->dropDownList(StopList::$types) ?>

    <?php echo $form->field($model, 'value')->textInput(['maxlength' => true])->hint(Yii::t('backend','Simple word or PCRE regular expression')) ?>
    
    <?php echo $form->field($model, 'description')->textArea(['maxlength' => true]) ?>
    
    <?php echo $form->field($model, 'is_regexp')->checkbox() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
