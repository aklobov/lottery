<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StopList */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Stop List',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Stop List'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="black-list-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
