<?php

use common\grid\EnumColumn;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
        echo Html::a(Yii::t('backend', 'Create {modelClass}', [
                    'modelClass' => 'User',
                ]), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'username',
            'email:email',
            'phone',
            [
                'class' => EnumColumn::className(),
                'attribute' => 'status',
                'enum' => User::statuses(),
                'filter' => User::statuses()
            ],
            'userProfile.account:currency:Счёт',
            'created_at:datetime',
            'logged_at:datetime',
            // 'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {toggle}{account}',
                'buttons' => [
                    'toggle' => function($url, $model) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-off"></span>', $url, [
                                    'title' => User::STATUS_DELETED == $model->status ? 'Enable' : 'Disable',
                                    'data-pjax' => '0',
                        ]);
                    },
                            'account' => function($url, $model) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-usd"></span>', Url::to(['/account/index', 'AccountLogSearch[prefix]' => $model->id]), [
                                    'title' => 'Account history',
                                    'data-pjax' => '0',
                        ]);
                    }]
                    ],
                ],
            ]);
            ?>

</div>
