<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title                   = $model->getPublicIdentity();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        $newStatus                     = $model->status < User::STATUS_DELETED ? 'Disable' : 'Enable';
        echo Html::a(Yii::t('backend', $newStatus), ['toggle', 'id' => $model->id, 'view' => 'view'], [
            'class' => 'btn ' . ($model->status < User::STATUS_DELETED ? 'btn-danger' : 'btn-success'),
            'data'  => [
//                'confirm' => Yii::t('backend', 'Are you sure you want to disable this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?php
    echo DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'username',
            'userProfile.firstname:text',
            'userProfile.middlename:text',
            'userProfile.lastname:text',
            'userProfile.gender:text',
            'userProfile.locale:text',
//            'referrer_id:text',
            [
                'label'  => Yii::t('backend', 'Referrer'),
                'format' => 'html',
                'value'  => empty($model->referrer_id) ? '&mdash;' : Html::a($model->referrer->publicIdentity, Url::to(['user/view', 'id' => $model->referrer->id])),
            ],
            [
                'label'  => Yii::t('backend', 'Leader'),
                'format' => 'html',
                'value'  => empty($model->leader_id) ? '&mdash;' : Html::a($model->leader->publicIdentity, Url::to(['user/view', 'id' => $model->leader->id])),
            ],
            'referral_id:text',
//            'leader_id:text',
            'email:email',
            [
                'label'  => 'Status',
                'format' => 'html',
                'value'  => User::statuses($model->status),
            ],
            'userProfile.account:currency',
            'created_at:datetime',
            'updated_at:datetime',
            'logged_at:datetime',
        ],
    ])
    ?>

</div>
