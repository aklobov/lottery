PHP 5.6 (лучше 7)
Расширения php:
intl (>=1.1.0) с поддержкой ICU version >= 49.1
PDO MySQL extension
DOM extension 
GMP extension
MBString extension
Ctype extension 
SPL extension 
PCRE extension
GD PHP extension with FreeType support
Fileinfo extension
Json extension
PHP mail SMTP 
APC extension
Memcache extension
OpenSSL extension
Reflection extension 

MySQL >=5.7.8

Возможность запуска php скриптов по расписанию (command shell и cron).
